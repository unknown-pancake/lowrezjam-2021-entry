
const std = @import("std");
const core = @import("../../core.zig");
const game = @import("../../game.zig");
const lib = @import("../../lib.zig");

const InGame = @This();
const Engine = core.Engine;
const PlayerActor = game.actors.Player;



mainloop: Engine.Mainloop,

ldtk: core.LDTK,
level_renderer: core.LDTKLevelRenderer,
player: *PlayerActor,


pub fn new() anyerror!*InGame {
  var self = try std.heap.c_allocator.create(InGame);
  
  self.mainloop = .{ 
    .start_fn = onStart,
    .event_fn = onEvent,
    .tick_fn = onTick,
    .render_fn = onRender,
  };

  return self;
}

pub fn destroy(self: *InGame) void {
  self.ldtk.destroy();
  self.level_renderer.destroy();
  self.player.destroy();

  std.heap.c_allocator.destroy(self);
}


fn onStart(ml: *Engine.Mainloop) anyerror!void {
  var self = @fieldParentPtr(InGame, "mainloop", ml);

  try Engine.graphics().setSize(64, 64);

  self.ldtk = try core.LDTK.init("data/world/metroidvania_world.ldtk");
  var uid = self.ldtk.findLevelUID("Level_0").?;
  self.level_renderer = try core.LDTKLevelRenderer.init(&self.ldtk, try self.ldtk.fetchLevel(uid));

  self.player = try PlayerActor.new();
}

fn onEvent(ml: *Engine.Mainloop, ev: *const Engine.Event) anyerror!void {
  var self = @fieldParentPtr(InGame, "mainloop", ml);

  switch(ev.type) {
    lib.SDL_QUIT => {
      Engine.stop();
      self.destroy();
    },
    else => {}
  }
}

fn onTick(ml: *Engine.Mainloop) anyerror!void {
  var self = @fieldParentPtr(InGame, "mainloop", ml);

  var g = Engine.graphics();

  try self.player.tick();
  g.viewport.setCenter(self.player.position);
}

fn onRender(ml: *Engine.Mainloop) anyerror!void {
  var self = @fieldParentPtr(InGame, "mainloop", ml);

  var g = Engine.graphics();

  g.clear(255, 255, 255, 255);
  
  self.level_renderer.render();
  try self.player.render();
}
