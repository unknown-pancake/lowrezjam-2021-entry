
const std = @import("std");
const lib = @import("../../lib.zig");
usingnamespace @import("../../core.zig");

const Player = @This();


const MOVEMENT_SPEED = 30.0/60.0; // px/frame


const PlayerFSM = FSM(PlayerState);
const PlayerState = enum {
  idling,
  running,
};


position: Vec2,
anim: AsepriteAnim,

fsm: PlayerFSM,



pub fn new() anyerror!*Player {
  var self = try std.heap.c_allocator.create(Player);
  errdefer std.heap.c_allocator.destroy(self);

  self.position = Vec2.zero;

  self.anim = try AsepriteAnim.init(
    "data/actors/player/player.json", "idle", 
    Vec2.init(8, 8)
  );

  self.fsm = PlayerFSM.init();

  self.fsm.setState( .idling, .{ 
    .resume_fn = onResumeIdling,
    .tick_fn = onTickIdling,
    .render_fn = onRenderIdling,
  });

  self.fsm.setState( .running, .{
    .resume_fn = onResumeRunning,
    .tick_fn = onTickRunning,
    .render_fn = onRenderRunning,
  });

  try self.fsm.push(.idling);

  return self;
}

pub fn destroy(self: *Player) void {
  self.anim.destroy();
  self.fsm.destroy();
  std.heap.c_allocator.destroy(self);
}



pub fn tick(self: *Player) anyerror!void {
  try self.fsm.tick();
}

pub fn render(self: *Player) anyerror!void {
  try self.fsm.render();
}



fn getMovementInput() Vec2 {
  const inp = Engine.input();
  const left: f32 =  if( inp.isKeyPressed(.a) ) 1.0 else 0.0;
  const right: f32 = if( inp.isKeyPressed(.d) ) 1.0 else 0.0;
  const up: f32 =    if( inp.isKeyPressed(.w) ) 1.0 else 0.0;
  const down: f32 =  if( inp.isKeyPressed(.s) ) 1.0 else 0.0;

  return Vec2.init(right - left, down - up);
}



fn onResumeIdling(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.switchTo("idle");
}

fn onTickIdling(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.tick();

  if( getMovementInput().lengthSquared() > 0 ) {
    try fsm_.switchTo(.running);
    return;
  }
}

fn onRenderIdling(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.render();
}


fn onResumeRunning(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.switchTo("run");
}

fn onTickRunning(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.tick();

  var mov_inp = getMovementInput();

  if( mov_inp.lengthSquared() == 0 ) {
    try fsm_.switchTo(.idling);
    return;
  }

  var motion = mov_inp.normalize().scale(MOVEMENT_SPEED);
  _ = self.position.madd(motion);
  self.anim.setPosition(self.position);

  if( motion.x < 0 ) {
    self.anim.sprite.flip_h = true;
  } else if( motion.x > 0 ) {
    self.anim.sprite.flip_h = false;
  }
}

fn onRenderRunning(fsm_: *PlayerFSM) anyerror!void {
  var self = @fieldParentPtr(Player, "fsm", fsm_);
  self.anim.render();
}