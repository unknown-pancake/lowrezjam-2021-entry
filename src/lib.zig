
const std = @import("std");
pub usingnamespace @import("./lib/c.zig");


pub const SDLError = error {
    SDLError
};


pub fn checkSDLPointer(ptr: anytype) SDLError!std.meta.Child(@TypeOf(ptr)) {
    if( ptr ) |p| {
        return p;
    } else {
        std.log.err("SDL Error : {s}", .{ SDL_GetError() });
        return SDLError.SDLError;
    }
}

pub fn checkSDLSuccess0(v: c_int) SDLError!void {
    if( v != 0 ) {
        std.log.err("SDL Error : {s}", .{ SDL_GetError() });
        return SDLError.SDLError;
    }
}
