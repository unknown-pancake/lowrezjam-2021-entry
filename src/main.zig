
const std = @import("std");
const core = @import("./core.zig");
const game = @import("./game.zig");

const Engine = core.Engine;
const StartScreen = game.screens.StartScreen;



pub fn main() anyerror!void {
  var start_screen = try StartScreen.new();

  try Engine.initialise(std.heap.c_allocator, &start_screen.mainloop);
  defer Engine.shutdown();

  try Engine.run();
}
