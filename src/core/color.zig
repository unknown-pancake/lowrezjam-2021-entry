
const lib = @import("../lib.zig");

const Color = @This();

r: u8,
g: u8,
b: u8,
a: u8,


pub fn init(r: u8, g: u8, b: u8, a: u8) Color {
    return Color {
        .r = r,
        .g = g,
        .b = b,
        .a = a
    };
}


pub fn toSDL(self: Color) lib.SDL_Color {
    return @bitCast(lib.SDL_Color, self);
}

