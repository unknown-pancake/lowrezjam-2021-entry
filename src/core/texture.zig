
const std = @import("std");
const lib = @import("../lib.zig");
const core = @import("../core.zig");

const Texture = @This();
const Engine = core.Engine;
const Vec2 = core.Vec2;



texture: *lib.SDL_Texture,



pub fn create(w: u32, h: u32) lib.SDLError!Texture {
    return Texture {
        .texture = try lib.checkSDLPointer( 
            lib.SDL_CreateTexture(
                Engine.graphics().renderer, 
                lib.SDL_PIXELFORMAT_RGBA32, 
                lib.SDL_TEXTUREACCESS_TARGET,
                @intCast(c_int, w), @intCast(c_int, h)
            ) 
        )
    };
}

pub fn load(path: []const u8) anyerror!Texture {
    var buf = try std.heap.c_allocator.alloc(u8, path.len+1);
    buf[path.len] = 0;
    var cpath = buf[0..path.len :0];
    defer std.heap.c_allocator.free(buf);

    std.mem.copy(u8, cpath, path);
    cpath[path.len] = 0;

    var surf = lib.IMG_Load(cpath);
    if( surf == null ) {
        std.log.err("Unable to load texture '{s}': {s}", .{ path, lib.IMG_GetError() } );
        return lib.SDLError.SDLError;
    }
    defer lib.SDL_FreeSurface(surf);

    var texture = try lib.checkSDLPointer(
        lib.SDL_CreateTextureFromSurface(Engine.graphics().renderer, surf)
    );

    return Texture {
        .texture = texture
    };
}

pub fn destroy(self: *Texture) void {
    lib.SDL_DestroyTexture(self.texture);
}



pub fn getSize(self: Texture) Vec2 {
    var w: c_int = 0;
    var h: c_int = 0;

    _ = lib.SDL_QueryTexture(self.texture, null, null, &w, &h);

    return Vec2.init( @intToFloat(f32, w), @intToFloat(f32, h) );
}
