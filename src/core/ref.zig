
const std = @import("std");


pub fn Ref(comptime T: type, dtor: fn(v: *T) void) type {
  return struct {
    const Self = @This();

    value: T,
    count: usize,


    pub fn init(val: T)  Self {
      return Self {
        .value = val,
        .count = 1,
      };
    }

    pub fn destroy(self: *Self) void {
      if( self.count > 0 ) {
        std.log.warn("Destroying an alive Ref(" ++ @typeName(T) ++ ") !", .{});
        dtor(&self.value);
      }
    }


    pub fn alive(self: Self) bool {
      return self.count > 0;
    }


    pub fn obtain(self: *Self) *Self {
      self.ensureValid();

      self.count += 1;
      return self;
    }

    pub fn release(self: *Self) *Self {
      self.ensureValid();

      self.count -= 1;
      if( self.count == 0 )
        dtor(&self.value);
      
      return self;
    }


    pub fn ensureValid(self: Self) void {
      if( self.count == 0 )
        @panic("The Ref(" ++ @typeName(T) ++ ") is not valid!");
    }

  };

}
