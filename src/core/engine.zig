
const std = @import("std");
const builtin = @import("builtin");
const lib = @import("../lib.zig");
const core = @import("../core.zig");

const Engine = @This();
const Color = core.Color;
const Array = std.ArrayList;



pub const EngineInstance = @import("./engine/instance.zig");
pub const FsInstance = @import("./engine/fs.zig");
pub const Fiber = @import("./engine/fiber.zig").Fiber;
pub const TimeTracker = @import("./engine/time_tracker.zig");
pub const Graphics = @import("./engine/graphics.zig");
pub const Input = @import("./engine/input.zig");
pub const Mainloop = @import("./engine/mainloop.zig");
pub const Viewport = @import("./engine/viewport.zig");



pub const InitialisationError = EngineInstance.InitialisationError;
pub const BackgroundFiber = EngineInstance.BackgroundFiber;

pub const AllocatorError = std.mem.Allocator.Error;
pub const Event = lib.SDL_Event;



var _instance: ?*EngineInstance = null;



pub fn initialise(
  a_allocator: *std.mem.Allocator,
  a_mainloop: *Mainloop
) InitialisationError!void {
  _instance = try EngineInstance.new(a_allocator, a_mainloop);
}

pub fn shutdown() void {
  instance().destroy();
  _instance = null;
}



pub fn hasInstance() bool {
  return _instance != null;
}

pub fn instance() *EngineInstance {
  if( _instance ) |inst|
    return inst
  else
    @panic("Trying to access the engine before initializing it!");
}



pub fn allocator() *std.mem.Allocator {
  return instance().allocator;
}

pub fn graphics() *Graphics {
  return &instance().graphics;
}

pub fn input() *Input {
  return &instance().input;
}

pub fn timeTracker() *TimeTracker {
  return &instance().time_tracker;
}

pub fn tickCount() u64 {
  return instance().tick_count;
}



pub fn setMainloop(
  mainloop: *Mainloop
) void {
  instance().setMainloop(mainloop);
}

pub fn setFrameTimeDiagnosticDisplayEnabled(
  enabled: bool
) void {
  instance().setFrameTimeDiagnosticDisplayEnabled(enabled);
}



pub fn addFiber() AllocatorError!*BackgroundFiber {
  return instance().addFiber();
}



pub fn stop() void {
  instance().stop();
}

pub fn run() anyerror!void {
  try instance().run();
}
