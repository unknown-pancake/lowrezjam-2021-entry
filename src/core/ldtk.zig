
const std = @import("std");
const core = @import("../core.zig");

const LDTK = @This();

const json_parse_opts = std.json.ParseOptions {
    .allocator = std.heap.c_allocator,
    .ignore_unknown_fields = true,
};

pub const LDTKError = error {
    LeveLNotFound
};


base_directory: []const u8,
data: Project,



pub fn init(path: []const u8) !LDTK {
    @setEvalBranchQuota(10000);
    const fs = std.fs;
    const json = std.json;

    var content = try fs.cwd().readFileAlloc(std.heap.c_allocator, path, 10_000_000);
    defer std.heap.c_allocator.free(content);

    var stream = json.TokenStream.init(content);
    var data = try json.parse(Project, &stream, json_parse_opts);

    return LDTK {
        .base_directory = fs.path.dirname(path).?,
        .data = data
    };
}

pub fn destroy(self: *LDTK) void {
    std.json.parseFree(Project, self.data, json_parse_opts);
}


pub fn getRelativePath(self: *LDTK, path: []const u8) ![]const u8 {
    return try std.fs.path.join(std.heap.c_allocator, &.{
        self.base_directory,
        path
    });
}


pub fn findLevelUID(self: *LDTK, id: []const u8) ?u32 {
    for( self.data.levels ) |level| {
        if( std.mem.eql(u8, id, level.identifier) )
            return level.uid;
    }

    return null;
}

pub fn fetchLevel(self: *LDTK, uid: u32) !Level {
    for( self.data.levels ) |*level| {
        if( level.uid == uid ) {
            if( !self.data.externalLevels )
                return level.*;
            
            return self.loadLevel(level);
        }
    }

    return LDTKError.LeveLNotFound;
}

fn loadLevel(self: *LDTK, lvl: *const Level) !Level {
    const fs = std.fs;
    const json = std.json;

    var path = try self.getRelativePath(lvl.externalRelPath.?);
    defer std.heap.c_allocator.free(path);

    var content = try fs.cwd().readFileAlloc(std.heap.c_allocator, path, 10_000_000);
    defer std.heap.c_allocator.free(content);

    var stream = json.TokenStream.init(content);
    return try json.parse(Level, &stream, json_parse_opts);
}




pub const Layout = enum {
    Free, GridVania, LinearHorizontal, LinearVertical
};

pub const Direction = enum {
    n, s, w, e
};

pub const LayerType = enum {
    IntGrid, Entities, Tiles, AutoLayer
};

pub const FieldType = enum {
    Int, 
    Float, 
    Bool, 
    String, 
    FilePath, 
    Color, 
    Point,
    IntArray,
    FloatArray,
    StringArray,
    FilePathArray,
    ColorArray,
    PointArray,
};

pub const FieldValue = union(FieldType) {
    Int: i32,
    Float: f32,
    Bool: bool,
    String: []u8,
    FilePath: []u8, // would be parsed as String
    Color: []u8,    // would be parsed as String
    Point: [2]i32,
    IntArray: []i32,
    FloatArray: []f32,
    StringArray: [][]u8,
    FilePathArray: [][]u8,    // would be parsed as StringArray
    ColorArray: [][]u8,       // would be parsed as ColorArray
    PointArray: [][2]i32,
};


pub const Level = struct {
    __bgColor: []u8,
    __bgPos: ?LevelBgPos,
    __neighbours: []LevelNeighbour,
    bgRelPath: ?[]u8,
    externalRelPath: ?[]u8,
    fieldInstances: []FieldInstance,
    identifier: []u8,
    layerInstances: ?[]LayerInstance,
    pxHei: u32,
    pxWid: u32,
    uid: u32,
    worldX: i32,
    worldY: i32
};

pub const LevelBgPos = struct {
    cropRect: [4]f32,
    scale: [2]f32,
    topLeftPx: [2]u32
};

pub const LevelNeighbour = struct {
    dir: Direction,
    levelUid: u32
};

pub const FieldInstance = struct {
    __identifier: []u8,
    __type: FieldType,
    __value: FieldValue,
    defUid: u32,
};

pub const LayerInstance = struct {
    __cHei: u32,
    __cWid: u32,
    __gridSize: u32,
    __identifier: []u8,
    __opacity: f32,
    __pxTotalOffsetX: i32,
    __pxTotalOffsetY: i32,
    __tilesetRelPath: ?[]u8,
    __type: LayerType,
    autoLayerTiles: []TileInstance,
    entityInstances: []EntityInstance,
    gridTiles: []TileInstance,
    intGridCsv: []u32,
    layerDefUid: u32,
    levelId: u32,
    overrideTilesetUid: ?u32,
    pxOffsetX: i32,
    pxOffsetY: i32,
    visible: bool,

    pub fn getTiles(self: *LayerInstance) ?[]TileInstance {
        return switch( self.__type ) {
            .Tiles => self.gridTiles,
            .AutoLayer => self.autoLayerTiles,
            else => null,
        };
    }
};

pub const TileInstance = struct {
    f: u32,
    px: [2]i32,
    src: [2]u32,
    t: u32
};

pub const EntityInstance = struct {
    __grid: [2]u32,
    __identifier: []u8,
    __pivot: [2]f32,
    __tile: ?EntityTile,
    defUid: u32,
    fieldInstances: []FieldInstance,
    height: u32,
    px: [2]i32,
    width: u32,
};

pub const EntityTile = struct {
    srcRect: [4]u32,
    tilesetUid: u32
};

pub const Project = struct {
    bgColor: []const u8,
    externalLevels: bool,
    jsonVersion: []const u8,
    levels: []const Level,
    worldGridHeight: u32,
    worldGridWidth: u32,
    worldLayout: Layout,
};
