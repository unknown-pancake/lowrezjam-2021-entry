
const std = @import("std");
const core = @import("../core.zig");


const LDTKLevelRenderer = @This();
const Engine = core.Engine;
const Texture = core.Texture;
const LDTK = core.LDTK;
const Level = LDTK.Level;
const LayerInstance = LDTK.LayerInstance;
const Vec2 = core.Vec2;

const Array = std.ArrayList;


level: Level,
layers: []LayerRenderer,


pub fn init(ldtk: *LDTK, lvl: Level) anyerror!LDTKLevelRenderer {
  var count = lvl.layerInstances.?.len;
  var layers = try Array(LayerRenderer).initCapacity(std.heap.c_allocator, count);
  defer layers.deinit();

  var i: usize = 0;
  while( i < count ) : (i += 1) {
    var layer = lvl.layerInstances.?[i];
    if( layer.getTiles() != null ) 
      try layers.append( try LayerRenderer.init(ldtk, lvl, &lvl.layerInstances.?[i]) );
  }
  
  count = layers.items.len;
  var reversed_layers = try std.heap.c_allocator.alloc(LayerRenderer, count);

  var k: isize = @intCast(isize, count - 1);
  var j: usize = 0;
  while( k >= 0 ) : (k -= 1) {
    reversed_layers[@intCast(usize, k)] = layers.items[j];
    j += 1;
  }

  return LDTKLevelRenderer {
    .level = lvl,
    .layers = reversed_layers
  };
}

pub fn destroy(self: *LDTKLevelRenderer) void {
  for( self.layers ) |*layer| 
    layer.destroy();
  
  std.heap.c_allocator.free(self.layers);
}


pub fn render(self: *LDTKLevelRenderer) void {
  for( self.layers ) |layer|
    layer.render();
}



pub const LayerRenderer = struct {
  layer: *LayerInstance,
  texture: Texture,
  offset: Vec2,

  pub fn init(ldtk: *LDTK, lvl: Level, layer: *LayerInstance) anyerror!LayerRenderer {
    var tex = try Texture.load(try ldtk.getRelativePath(layer.__tilesetRelPath.?));

    var offset = Vec2.init(
      @intToFloat(f32, lvl.worldX + layer.__pxTotalOffsetX),
      @intToFloat(f32, lvl.worldY + layer.__pxTotalOffsetY)
    );


    return LayerRenderer {
      .layer = layer,
      .texture = tex,
      .offset = offset,
    };
  }

  pub fn destroy(self: *LayerRenderer) void {
    self.texture.destroy();
  }


  pub fn render(self: *const LayerRenderer) void {
    var opttiles = self.layer.getTiles();
    var g = Engine.graphics();

    if( opttiles ) |tiles|
      for( tiles ) |tile|
        g.sprite(
          self.texture, 

          self.offset.x + @intToFloat(f32, tile.px[0]),
          self.offset.y + @intToFloat(f32, tile.px[1]),
          @intToFloat(f32, self.layer.__gridSize),
          @intToFloat(f32, self.layer.__gridSize),

          @intToFloat(f32, tile.src[0]),
          @intToFloat(f32, tile.src[1]),
          @intToFloat(f32, self.layer.__gridSize),
          @intToFloat(f32, self.layer.__gridSize),

          0, 0, 0, 
          tile.f & 1 == 1,
          tile.f & 2 == 2,
          255, 255, 255, 255
        );
  }

};
