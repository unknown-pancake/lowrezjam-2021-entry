
const math = @import("std").math;
const Vec2 = @This();



pub const zero = Vec2.init(0, 0);



x: f32,
y: f32,


pub fn init(x: f32, y: f32) Vec2 {
    return Vec2 {
        .x = x,
        .y = y
    };
}



pub fn lengthSquared(self: Vec2) f32 {
    return self.x*self.x + self.y*self.y;
}

pub fn length(self: Vec2) f32 {
    return math.sqrt(self.lengthSquared());
}

pub fn normalize(self: Vec2) Vec2 {
    var len = self.lengthSquared();
    if( len > 0 ) {
        len = 1 / math.sqrt(len);
        return Vec2.init(self.x * len, self.y * len);
    }

    return self;
}


pub fn distanceSquared(self: Vec2, v: Vec2) f32 {
    return self.sub(v).lengthSquared();
}

pub fn distance(self: Vec2, v: Vec2) f32 {
    return math.sqrt(self.distanceSquared(v));
}


pub fn scale(self: Vec2, f: f32) Vec2 {
    return Vec2.init(self.x * f, self.y * f);
}


pub fn add(self: Vec2, v: Vec2) Vec2 {
    return Vec2.init(self.x + v.x, self.y + v.y);
}

pub fn sub(self: Vec2, v: Vec2) Vec2 {
    return Vec2.init(self.x - v.x, self.y - v.y);
}

pub fn mul(self: Vec2, v: Vec2) Vec2 {
    return Vec2.init(self.x * v.x, self.y * v.y);
}

pub fn div(self: Vec2, v: Vec2) Vec2 {
    return Vec2.init(self.x / v.x, self.y / v.y);
}

pub fn neg(self: Vec2) Vec2 {
    return Vec2.init(-self.x, -self.y);
}


pub fn madd(self: *Vec2, v: Vec2) *Vec2 {
    self.* = self.add(v);
    return self;
}

pub fn msub(self: *Vec2, v: Vec2) *Vec2 {
    self.* = self.sub(v);
    return self;
}

pub fn mmul(self: *Vec2, v: Vec2) *Vec2 {
    self.* = self.mul(v);
    return self;
}

pub fn mdiv(self: *Vec2, v: Vec2) *Vec2 {
    self.* = self.div(v);
    return self;
}

pub fn mneg(self: *Vec2) *Vec2 {
    self.* = self.neg();
    return self;
}
