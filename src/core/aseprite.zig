
const std = @import("std");
const core = @import("../core.zig");

const Aseprite = @This();
const Sprite = core.Engine.Graphics.Sprite;
const Texture = core.Texture;


const json_parse_opts = std.json.ParseOptions {
    .allocator = std.heap.c_allocator,
    .ignore_unknown_fields = true,
};


base_directory: []const u8,
data: Data,



pub fn init(path: []const u8) !Aseprite {
  @setEvalBranchQuota(10000);
  const fs = std.fs;
  const json = std.json;

  var content = try fs.cwd().readFileAlloc(std.heap.c_allocator, path, 1_000_000);
  defer std.heap.c_allocator.free(content);

  var stream = json.TokenStream.init(content);
  var data = try json.parse(Data, &stream, json_parse_opts);

  return Aseprite {
    .base_directory = fs.path.dirname(path).?,
    .data = data
  };
}

pub fn destroy(self: *Aseprite) void {
  std.json.parseFree(Data, self.data, json_parse_opts);
}



pub fn getRelativePath(self: *Aseprite, path: []const u8) ![]const u8 {
  return try std.fs.path.join(std.heap.c_allocator, &.{
    self.base_directory,
    path
  });
}


pub fn findFrameTags(self: *Aseprite, name: []const u8) ?FrameTag {
  for( self.data.meta.frameTags ) |ftag| {
    if( std.mem.eql(u8, name, ftag.name) )
      return ftag;
  }

  return null;
}



pub fn fetchFrame(self: *Aseprite, id: u32, spr: *Sprite) void {
  var x = @intToFloat(f32, self.data.frames[id].frame.x);
  var y = @intToFloat(f32, self.data.frames[id].frame.y);
  var w = @intToFloat(f32, self.data.frames[id].frame.w);
  var h = @intToFloat(f32, self.data.frames[id].frame.h);

  spr.source = core.Rect.init(x, y, w, h);
}

pub fn getFrameDuration(self: *Aseprite, id: u32) u32 {
  return self.data.frames[id].duration * std.time.ns_per_ms;
}

pub fn loadTexture(self: *Aseprite) !Texture {
  var path = try self.getRelativePath(self.data.meta.image);
  defer std.heap.c_allocator.free(path);

  return try Texture.load(path);
}



pub const Data = struct {
  frames: []Frame,
  meta: Meta,
};

pub const Frame = struct {
  filename: []u8,
  frame: Rect,
  rotated: bool,
  trimmed: bool,
  spriteSourceSize: Rect,
  sourceSize: Size,
  duration: u32,
};

pub const Rect = struct {
  x: u32, y: u32, w: u32, h: u32
};

pub const Size = struct {
  w: u32, h: u32
};

pub const Meta = struct {
  image: []u8,
  frameTags: []FrameTag
};

pub const FrameTag = struct {
  name: []u8,
  from: u32,
  to: u32,
  direction: Direction,
};

pub const Direction = enum {
  forward,
  reverse,
  pingpong,
};
