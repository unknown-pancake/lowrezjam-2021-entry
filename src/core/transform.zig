
const math = @import("std").math;
const core = @import("../core.zig");

const Transform = @This();
const Vec2 = core.Vec2;
const Rect = core.Rect;


pub const identity = Transform.initIdentity();


matrix: [16]f32,



pub fn initIdentity() Transform {
  return Transform { 
    .matrix = .{ 
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1
    }
  };
}

pub fn init(
  a00: f32, a01: f32, a02: f32, 
  a10: f32, a11: f32, a12: f32, 
  a20: f32, a21: f32, a22: f32, 
) Transform {
  return Transform {
    .matrix = .{
      a00, a10, 0,   a20, 
      a01, a11, 0,   a21,
      0,   0,   1,   0,
      a02, a12, 0,   a22
    }
  };
}


pub fn getDeterminant(self: *Transform) f32 {
  const m = self.matrix;

  return m[0] * (m[15] * m[5] - m[7] * m[13]) -
         m[1] * (m[15] * m[4] - m[7] * m[12]) +
         m[3] * (m[13] * m[4] - m[5] * m[12]);
}

pub fn getInverse(self: *Transform) Transform {
  var det = self.getDeterminant();

  if( det != 0 ) {
    const m = self.matrix;

    return Transform.init( 
       (m[15] * m[5] - m[7] * m[13]) / det,
      -(m[15] * m[4] - m[7] * m[12]) / det,
       (m[13] * m[4] - m[5] * m[12]) / det,
      -(m[15] * m[1] - m[3] * m[13]) / det,
       (m[15] * m[0] - m[3] * m[12]) / det,
      -(m[13] * m[0] - m[1] * m[12]) / det,
       (m[7]  * m[1] - m[3] * m[5])  / det,
      -(m[7]  * m[0] - m[3] * m[4])  / det,
       (m[5]  * m[0] - m[1] * m[4])  / det,
    );
  }

  return identity;
}



pub fn combine(self: *Transform, t: Transform) *Transform {
  const a = self.matrix;
  const b = t.matrix;

  self.* = Transform.init(
    a[0] * b[0]  + a[4] * b[1]  + a[12] * b[3],
    a[0] * b[4]  + a[4] * b[5]  + a[12] * b[7],
    a[0] * b[12] + a[4] * b[13] + a[12] * b[15],
    a[1] * b[0]  + a[5] * b[1]  + a[13] * b[3],
    a[1] * b[4]  + a[5] * b[5]  + a[13] * b[7],
    a[1] * b[12] + a[5] * b[13] + a[13] * b[15],
    a[3] * b[0]  + a[7] * b[1]  + a[15] * b[3],
    a[3] * b[4]  + a[7] * b[5]  + a[15] * b[7],
    a[3] * b[12] + a[7] * b[13] + a[15] * b[15],
  );

  return self;
}

pub fn translate(self: *Transform, x: f32, y: f32) *Transform {
  const t = Transform.init( 
    1, 0, x, 
    0, 1, y,
    0, 0, 1
  );

  return self.combine(t);
}

pub fn translateV(self: *Transform, v: Vec2) *Transform {
  return self.translate(v.x, v.y);
}

pub fn rotate(self: *Transform, angle: f32, cx: f32, cy: f32) *Transform {
  const c = math.cos(angle);
  const s = math.sin(angle);

  const t = Transform.init(
    c, -s, cx * (1 - c) + cy * s,
    s,  c, cy * (1 - c) - cx * s,
    0,  0, 1
  );

  return self.combine(t);
}

pub fn rotateV(self: *Transform, angle: f32, c: Vec2) *Transform {
  return self.rotate(angle, c.x, c.y);
}

pub fn scale(self: *Transform, x: f32, y: f32) *Transform {
  const t = Transform.init(
    x, 0, 0, 
    0, y, 0,
    0, 0, 1
  );

  return self.combine(t);
}

pub fn scaleV(self: *Transform, v: Vec2) *Transform {
  return self.scale(v.x, v.y);
}

pub fn scaleCentered(self: *Transform, sx: f32, sy: f32, cx: f32, cy: f32) *Transform {
  const t = Transform.init(
    sx, 0,  cx * (1 - sx), 
    0,  sy, cy * (1 - sy),
    0,  0,  1
  );

  return self.combine(t);
}

pub fn scaleCenteredV(self: *Transform, s: Vec2, c: Vec2) *Transform {
  return self.scaleCentered(s.x, s.y, c.x, c.y);
}



pub fn transformPoint(self: *Transform, x: f32, y: f32) Vec2 {
  const m = self.matrix;

  return Vec2.init(
    m[0] * x + m[4] * y + m[12],
    m[1] * x + m[5] * y + m[13]
  );
}

pub fn transformVec2(self: *Transform, v: Vec2) Vec2 {
  return self.transformPoint(v.x, v.y);
}

pub fn transformRect(self: *Transform, x: f32, y: f32, w: f32, h: f32) Rect {
  const tl = self.transformPoint(x, y);
  const br = self.transformPoint(x+w, y+h);

  return Rect.init(
    tl.x, tl.y, 
    br.x - tl.x, br.y - tl.y
  );
}

pub fn transformRectR(self: *Transform, r: Rect) Rect {
  return self.transformRect(r.position.x, r.position.y, r.size.x, r.size.y);
}
