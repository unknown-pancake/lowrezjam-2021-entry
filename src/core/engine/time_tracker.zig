
const std = @import("std");
const Array = std.ArrayList;
const Timer = std.time.Timer;
const Allocator = std.mem.Allocator;

const TimeTracker = @This();



allocator: *Allocator,
timer: Timer,
root: Frame,
stack: Array(*Frame),
depth: u32,



pub fn init(
  allocator: *Allocator,
) Timer.Error!TimeTracker {
  return TimeTracker {
    .allocator = allocator,
    .timer = try Timer.start(),
    .root = Frame { 
      .start = 0, .end = 0, 
      .name = &.{}, 
      .frames = Array(Frame).init(allocator) 
    },
    .stack = Array(*Frame).init(allocator),
    .depth = 0,
  };
}

pub fn destroy(
  self: *TimeTracker
) void {
  self.root.free();
  self.stack.deinit();
}


pub fn nowTimestamp(
  self: TimeTracker
) u64 {
  return self.timer.read();
}


pub fn startFrame(
  self: *TimeTracker, 
  name: []const u8
) Allocator.Error!void {
  var now_timestamp = self.nowTimestamp();

  var frame = Frame {
    .start = now_timestamp,
    .end = now_timestamp,
    .name = name, 
    .frames = Array(Frame).init(self.allocator)
  };

  if( self.stack.items.len == 0 )
    try self.pSetupRootFrame(frame)
  else
    try self.pPushFrame(frame);
}

pub fn updateCurrentFrame(self: *TimeTracker) void {
  var frame = self.stack.items[ self.stack.items.len - 1 ];
  frame.end = self.nowTimestamp();
}

pub fn endFrame(self: *TimeTracker) void {
  var frame = self.stack.pop();
  frame.end = self.nowTimestamp();
  self.depth -= 1;
}

pub fn endAllFrames(self: *TimeTracker) void {
  if( self.stack.items.len > 0 ) {
    std.log.warn("*** Time tracking structure is invalid! ***", .{});
    std.log.warn("{} time frames were started but not ended!", .{ self.stack.items.len });

    while( self.stack.items.len > 0 )
      self.endFrame();
  }
}


pub fn current(self: TimeTracker) *Frame {
    return self.stack.items[self.stack.items.len - 1];
}



fn pSetupRootFrame(
  self: *TimeTracker,
  frame: Frame 
) Allocator.Error!void {
  self.root.free();
  self.root = frame;

  try self.stack.append(&self.root);

  self.depth = 1;
}

fn pPushFrame(
  self: *TimeTracker,
  frame: Frame
) Allocator.Error!void {
  var stack = &self.stack.items;
  var parent = stack.*[stack.len - 1];
  var parent_frames = &parent.frames.items;

  try parent.frames.append(frame);
  try self.stack.append(&parent_frames.*[parent_frames.len - 1]);

  if( self.depth < self.stack.items.len )
    self.depth = @intCast(u32, self.stack.items.len);
}



pub const Frame = struct {
    start: u64,
    end: u64,
    name: []const u8,
    frames: Array(Frame),

    pub fn duration(self: Frame) u64 {
        return self.end - self.start;
    }

    pub fn innerDuration(self: Frame) u64 {
        if( self.frames.items.len == 0 ) {
            return 0;
        }
        
        const s = self.frames.items[0].start;
        const e = self.frames.items[self.frames.items.len-1].end;
        return e - s;
    }


    pub fn free(self: *Frame) void {
        for(self.frames.items) |*frame| {
            frame.free();
        }

        self.frames.deinit();
    }

    pub fn freeRetainingCapacity(self: *Frame) void {
        for(self.frames.items) |*frame| {
            frame.free();
        }

        self.frames.clearRetainingCapacity();
    }
};



