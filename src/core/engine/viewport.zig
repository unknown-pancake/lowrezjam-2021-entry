
const core = @import("../../core.zig");

const Viewport = @This();
const Vec2 = core.Vec2;
const Rect = core.Rect;
const Transform = core.Transform;
const Engine = core.Engine;



center: Vec2,
zoom: f32,
transform: ?Transform,
inverse_transform: ?Transform,



pub fn init() Viewport {
  return Viewport {
    .center = Vec2.zero,
    .zoom = 1,
    .transform = null,
    .inverse_transform = null,
  };
}



pub fn setCenter(
  self: *Viewport, 
  new_center: Vec2
) void {
  self.center = new_center;
  self.pInvalidateTransforms();
}

pub fn setZoom(
  self: *Viewport, 
  new_zoom: f32
) void {
  self.zoom = new_zoom;
  self.pInvalidateTransforms();
}



pub fn getRectangle(
  self: *const Viewport
) Rect {
  var rectangle = Rect.initV(
    Vec2.zero,
    Engine.getGraphics().getSize().scale(1 / self.zoom)
  );

  rectangle.setCenter(self.center);

  return rectangle;
}



pub fn transformPoint(
  self: *Viewport, 
  x: f32, y: f32
) Vec2 {
  return self.transformPointV( Vec2.init(x, y) );
}

pub fn transformPointV(
  self: *Viewport, 
  point: Vec2
) Vec2 {
  return self.pComputeTransform().transformVec2(point);
}



pub fn transformRect(
  self: *Viewport, 
  x: f32, y: f32, 
  width: f32, height: f32
) Rect {
  return self.transformRectR( Rect.init(x, y, width, height) );
}

pub fn transformRectR(
  self: *Viewport, 
  rectangle: Rect
) Rect {
  return self.pComputeTransform().transformRectR(rectangle);
}



fn pInvalidateTransforms(
  self: *Viewport
) void {
  self.transform = null;
  self.inverse_transform = null;
}

fn pComputeTransform(
  self: *Viewport
) *Transform {
  if( self.transform ) |*xform|
    return xform;

  const size = Engine.graphics().getSize();

  self.transform = Transform.initIdentity();

  _ = self.transform.?
    .translateV(size.scale(0.5))
    .scale(self.zoom, self.zoom)
    .translateV(self.center.neg());
  
  return &self.transform.?;
}
