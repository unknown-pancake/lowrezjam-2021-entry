
const EngineInstance = @This();

const std = @import("std");
const builtin = @import("builtin");
const lib = @import("../../lib.zig");
const core = @import("../../core.zig");

const Engine = @import("../engine.zig");
const Graphics = Engine.Graphics;
const Input = Engine.Input;
const TimeTracker = Engine.TimeTracker;
const Mainloop = Engine.Mainloop;

const AllocatorError = std.mem.Allocator.Error;
const BackgroundFiberArray = std.ArrayList(BackgroundFiber);



pub const BackgroundFiber = Engine.Fiber(void);

pub const InitialisationError = error {
  SDL2,
  SDL_img,
  Window,
} 
|| lib.SDLError 
|| std.time.Timer.Error
|| AllocatorError;



allocator: *std.mem.Allocator,
window: *lib.SDL_Window,
graphics: Graphics,
input: Input,
time_tracker: TimeTracker,
mainloop: *Mainloop,

mainloop_started: bool, 
running: bool,
tick_count: u64,

fibers_time_margin: u64,
fibers: BackgroundFiberArray,

diagnostics: packed struct {
  frame_time: bool
},



const default_fiber_time_margin = 4 * std.time.ns_per_ms;
const sdl_init_flags = 
    lib.SDL_INIT_VIDEO 
  | lib.SDL_INIT_GAMECONTROLLER 
  | lib.SDL_INIT_AUDIO;
const sdl_img_init_flags = lib.IMG_INIT_PNG;
const sdl_window_flags = lib.SDL_WINDOW_RESIZABLE;
const frame_duration_ns = std.time.ns_per_s / 60;


pub fn new(
  allocator: *std.mem.Allocator,
  mainloop: *Engine.Mainloop
) InitialisationError!*EngineInstance {
  try pInitializeSDL2();
  errdefer pShutdownSDL2();

  try pInitializeSDLIMG();
  errdefer pShutdownSDLIMG();

  var window = try pCreateWindow();
  errdefer pDestroyWindow(window);

  var graphics = try Graphics.init(window);
  errdefer graphics.destroy();

  var time_tracker = try TimeTracker.init(allocator);
  errdefer time_tracker.destroy();

  var instance = try allocator.create(EngineInstance);
  instance.* = EngineInstance {
    .allocator = allocator,
    .window = window,
    .graphics = graphics,
    .input = Input.init(),
    .time_tracker = time_tracker,
    .mainloop = mainloop,

    .mainloop_started = false,
    .running = false,
    .tick_count = 0,

    .fibers_time_margin = default_fiber_time_margin,
    .fibers = BackgroundFiberArray.init(allocator),

    .diagnostics = .{ 
      .frame_time = (builtin.mode == .Debug)
    }
  };

  return instance;
}

pub fn destroy(
  self: *EngineInstance
) void {
  self.fibers.deinit();

  self.time_tracker.destroy();
  self.graphics.destroy();

  pDestroyWindow(self.window);
  pShutdownSDLIMG();
  pShutdownSDL2();

  self.allocator.destroy(self);
}



pub fn setMainloop(
  self: *EngineInstance,
  mainloop: *Mainloop
) void {
  self.mainloop = mainloop;
  self.mainloop_started = false;
}

pub fn setFrameTimeDiagnosticDisplayEnabled(
  self: *EngineInstance,
  enabled: bool
) void {
  self.diagnostics.frame_time = enabled;
}



pub fn addFiber(
  self: *EngineInstance
) AllocatorError!*BackgroundFiber {
  const index = self.fibers.len;

  try self.fibers.append(BackgroundFiber.init());

  return &self.fibers.items[ index ];
}



pub fn stop(
  self: *EngineInstance
) void {
  self.running = false;
}

pub fn run( 
  self: *EngineInstance
) anyerror!void {
  self.running = true;

  while( self.running )
    try self.pFrame();
}



fn pInitializeSDL2() InitialisationError!void {
  lib.checkSDLSuccess0( lib.SDL_Init(sdl_init_flags) ) 
  catch 
    return InitialisationError.SDL2;
}

fn pShutdownSDL2() void {
  lib.SDL_Quit();
}



fn pInitializeSDLIMG() InitialisationError!void {
  const initialized = lib.IMG_Init( sdl_img_init_flags );

  if( initialized & sdl_img_init_flags != sdl_img_init_flags ) {
    std.log.crit("Unable to initialize SDL_img : {s}", .{ lib.IMG_GetError() });
    return InitialisationError.SDL_img;
  }
}

fn pShutdownSDLIMG() void {
  lib.IMG_Quit();
}



fn pCreateWindow() InitialisationError!*lib.SDL_Window {
  return lib.checkSDLPointer(
    lib.SDL_CreateWindow(
      "Metroidvania",
      lib.SDL_WINDOWPOS_UNDEFINED, lib.SDL_WINDOWPOS_UNDEFINED,
      1280, 720,
      sdl_window_flags
    )
  ) catch 
    InitialisationError.Window;
}

fn pDestroyWindow(
  w: *lib.SDL_Window
) void {
  lib.SDL_DestroyWindow(w);
}



fn pFrame(
  self: *EngineInstance
) anyerror!void {
  {
    try self.time_tracker.startFrame("frame");
    defer self.time_tracker.endFrame();

    try self.pStartMainloop();

    try self.time_tracker.startFrame("graphics.startFrame");
    try self.graphics.startFrame();
    self.time_tracker.endFrame();

    try self.pEvent();
    try self.pTick();
    try self.pRender();
    try self.pBackgroundFibers();

    try self.time_tracker.startFrame("graphics.endFrame");
    self.graphics.endFrame();
    self.time_tracker.endFrame();
  }

  self.time_tracker.endAllFrames();

  self.pRenderDiagnostics();
  self.pIdle();

  self.graphics.present();
  self.tick_count += 1;
}

fn pStartMainloop(
  self: *EngineInstance
) anyerror!void {
  if( self.mainloop_started )
    return;
  
  self.mainloop_started = true;

  if( self.mainloop.start_fn ) |start_fn|
    try start_fn(self.mainloop);
}

fn pEvent(
  self: *EngineInstance
) anyerror!void {
  try self.time_tracker.startFrame("events");
  defer self.time_tracker.endFrame();

  self.input.update();

  var ev: Engine.Event = undefined;

  loop: while( self.running and lib.SDL_PollEvent(&ev) == 1 ) {
    self.input.integrate(&ev);

    switch( builtin.mode ) {
      .Debug, .ReleaseSafe =>
        if( self.pDiagnosticEvent(&ev) )
          continue :loop,
      else => {}
    }

    try self.mainloop.event_fn(self.mainloop, &ev);
  }
}

fn pTick(
  self: *EngineInstance
) anyerror!void {
  if( !self.running )
    return;
  
  try self.time_tracker.startFrame("tick");
  defer self.time_tracker.endFrame();

  try self.mainloop.tick_fn(self.mainloop);
}

fn pRender(
  self: *EngineInstance
) anyerror!void {
  if( !self.running )
    return;
  
  try self.time_tracker.startFrame("render");
  defer self.time_tracker.endFrame();

  try self.mainloop.render_fn(self.mainloop);
}

fn pBackgroundFibers(
  self: *EngineInstance
) anyerror!void {
  if( !self.running )
    return;
  
  try self.time_tracker.startFrame("fibers");
  defer self.time_tracker.endFrame();

  var index: usize = 0;

  var fibers = &self.fibers.items;

  while( fibers.len > 0 and index < fibers.len ) {
    const time_used = self.time_tracker.root.innerDuration();

    if( time_used > frame_duration_ns - self.fibers_time_margin )
      break;
    
    switch( fibers.*[index] ) {
      .empty, .succeeded => {
        _ = self.fibers.orderedRemove(index);
      },
      .awaiting => |frame| {
        try self.time_tracker.startFrame("fiber");
        defer self.time_tracker.endFrame();

        resume frame;
      },
      .awaiting_next_frame => |frame| {
        fibers.*[index] = BackgroundFiber{ 
          .awaiting = frame 
        };
        index += 1;
      },
      .failed => |err| {
        std.log.crit("A fiber failed with the error '{}'", .{ err } );
        return err;
      }
    }

    self.time_tracker.updateCurrentFrame();
  }
}



fn pDiagnosticEvent(
  self: *EngineInstance,
  event: *const Engine.Event
) bool {
  switch( event.type ) {
    lib.SDL_KEYDOWN => 
      switch( @intToEnum(Engine.Input.Scancode, event.key.keysym.scancode) ) {
        .f1 => {
          self.diagnostics.frame_time = !self.diagnostics.frame_time;
          return true;
        },
        else => {}
      },
    else => {}
  }

  return false;
}

fn pRenderDiagnostics(
  self: *EngineInstance
) void {
  if( self.diagnostics.frame_time )
    self.pRenderFrameTimeDiagnostic();
}

fn pIdle(
  self: *EngineInstance
) void {
  const time_used = self.time_tracker.root.duration();
  
  if( time_used < frame_duration_ns )
    std.time.sleep( frame_duration_ns - time_used );
}



fn pRenderFrameTimeDiagnostic(
  self: *EngineInstance
) void {
  const height = @intToFloat(f32, (10+1) * self.time_tracker.depth);
  const size = self.graphics.getScreenSize();

  _ = self.graphics.solid(1, size.y-2-height, size.x-2, height+1, 0, 0, 0, 127);

  self.pRenderFrameTimeDiagnosticRenderFrame(&self.time_tracker.root, size.y-2-10, 0);
}

fn pRenderFrameTimeDiagnosticRenderFrame(
  self: *EngineInstance, 
  frame: *const TimeTracker.Frame,
  y: f32, 
  i: usize,
) void {
  const colors = [_]core.Color {
    core.Color.init(255, 0, 0, 255),
    core.Color.init(0, 255, 0, 255),
    //core.Color.init(0, 0, 255, 255),
    core.Color.init(255, 255, 0, 255),
    core.Color.init(255, 128, 0, 255),
    core.Color.init(128, 255, 0, 255),
    core.Color.init(255, 0, 255, 255),
    core.Color.init(255, 0, 128, 255),
    core.Color.init(128, 0, 255, 255),
    core.Color.init(0, 255, 255, 255),
    core.Color.init(0, 255, 128, 255),
    core.Color.init(0, 128, 255, 255),
  };

  var g = self.graphics;
  const root_x = 1 + 1;
  const width = g.getScreenSize().x - 4;
  const height = 10;
  const start = self.time_tracker.root.start;

  const px_per_ns = width / frame_duration_ns;
  const x = root_x + px_per_ns*@intToFloat(f32, frame.start - start);
  const w = px_per_ns*@intToFloat(f32, frame.duration());
  const color = colors[i % colors.len];

  _ = g.solidC(x, y, w, height, color);

  for(frame.frames.items) |*f, j| {
    self.pRenderFrameTimeDiagnosticRenderFrame(f, y-height-1, j);
  }
}
