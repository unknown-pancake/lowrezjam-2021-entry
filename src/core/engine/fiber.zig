
pub const FiberError = error {
  FiberNotAwaiting,
};



pub fn Fiber(
  comptime T: type
) type {

  return union(enum) {
    const Self = @This();
    const FrameT = anyframe->T;



    empty: void,
    awaiting: FrameT,
    awaiting_next_frame: FrameT,
    succeeded: T,
    failed: anyerror,



    pub fn init() Self {
      return Self { .empty = {} };
    }



    pub fn clear(
      self: *Self
    ) void {
      self.* = Self { 
        .empty 
      };
    }

    pub fn waitFor(
      self: *Self, 
      frame: FrameT
    ) void {
      self.* = Self { 
        .awaiting = frame 
      };
    }

    pub fn waitForNextFrame(
      self: *Self
    ) FiberError!void {
      self.* = Self { 
        .awaiting_next_frame = try self.getFrame() 
      };
    }

    pub fn stopWaitingForNextFrame(
      self: *Self
    ) FiberError!void {
      self.* = Self { 
        .awaiting = try self.getFrame() 
      };
    }

    pub fn succeed(
      self: *Self, 
      value: T
    ) void {
      self.* = Self { 
        .succeeded = value 
      };
    }

    pub fn fail(
      self: *Self, 
      err: anyerror
    ) void {
      self.* = Self { 
        .failed = err 
      };
    }



    pub fn getFrame(
      self: Self
    ) FiberError!FrameT {
      return switch( self ) {
        .awaiting => |f| f,
        .awaiting_next_frame => |f| f,
        else => FiberError.FiberNotAwaiting
      };
    }

  };
  
}
