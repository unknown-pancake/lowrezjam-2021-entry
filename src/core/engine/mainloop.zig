
const Engine = @import("../engine.zig");
const Mainloop = @This();



pub const Fn = fn(self: *Mainloop) anyerror!void;
pub const EventFn = fn(self: *Mainloop, event: *const Engine.Event) anyerror!void;



start_fn: ?Fn,
event_fn: EventFn,
tick_fn: Fn,
render_fn: Fn,



pub fn init(
  start_fn: ?Fn, 
  event_fn: EventFn, 
  tick_fn: Fn, 
  render_fn: Fn
) Mainloop {
  return Mainloop { 
    .start_fn = start_fn,
    .event_fn = event_fn,
    .tick_fn = tick_fn,
    .render_fn = render_fn,
  };
}
