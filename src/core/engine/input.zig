
const std = @import("std");
const lib = @import("../../lib.zig");
const core = @import("../../core.zig");

const Input = @This();



pub const ButtonState = enum {
  released,
  just_pressed,
  pressed,
  just_released
};



keys: [lib.SDL_NUM_SCANCODES]ButtonState,



pub fn init() Input {
  var self = Input {
    .keys = undefined
  };

  std.mem.set(ButtonState, &self.keys, ButtonState.released);

  return self;
}



pub fn integrate(
  self: *Input, 
  event: *const lib.SDL_Event
) void {
  switch( event.type ) {
    lib.SDL_KEYDOWN =>
      self.keys[event.key.keysym.scancode] = .just_pressed,
    lib.SDL_KEYUP =>
      self.keys[event.key.keysym.scancode] = .just_released,
    else => {}
  }
}

pub fn update(
  self: *Input
) void {
  for( self.keys ) |*key| {
    switch( key.* ) {
      .just_pressed  => key.* = .pressed,
      .just_released => key.* = .released,
      else => {}
    }
  }
}



pub fn isKeyPressed(
  self: *Input, 
  key: Scancode
) bool {
  return switch( self.keys[@enumToInt(key)] ) {
    .just_pressed, .pressed => true,
    else => false
  };
}

pub fn isKeyJustPressed(
  self: *Input, 
  key: Scancode
) bool {
  return self.keys[@enumToInt(key)] == .just_pressed;
}

pub fn isKeyReleased(
  self: *Input, 
  key: Scancode
) bool {
  return switch( self.keys[@enumToInt(key)] ) {
    .just_released, .released => true,
    else => false,
  };
}

pub fn isKeyJustReleased(
  self: *Input, 
  key: Scancode
) bool {
  return self.keys[@enumToInt(key)] == .just_released;
}



pub const Scancode = enum(u32) {
    unknown = 0,
    a = 4,
    b = 5,
    c = 6,
    d = 7,
    e = 8,
    f = 9,
    g = 10,
    h = 11,
    i = 12,
    j = 13,
    k = 14,
    l = 15,
    m = 16,
    n = 17,
    o = 18,
    p = 19,
    q = 20,
    r = 21,
    s = 22,
    t = 23,
    u = 24,
    v = 25,
    w = 26,
    x = 27,
    y = 28,
    z = 29,
    d1 = 30,
    d2 = 31,
    d3 = 32,
    d4 = 33,
    d5 = 34,
    d6 = 35,
    d7 = 36,
    d8 = 37,
    d9 = 38,
    d0 = 39,
    enter = 40,
    escape = 41,
    backspace = 42,
    tab = 43,
    space = 44,
    minus = 45,
    equals = 46,
    left_bracket = 47,
    right_bracket = 48,
    backslash = 49,
    non_us_hash = 50,
    semicolon = 51,
    apostrophe = 52,
    grave = 53,
    comma = 54,
    period = 55,
    slash = 56,
    caps_lock = 57,
    f1 = 58,
    f2 = 59,
    f3 = 60,
    f4 = 61,
    f5 = 62,
    f6 = 63,
    f7 = 64,
    f8 = 65,
    f9 = 66,
    f10 = 67,
    f11 = 68,
    f12 = 69,
    print_screen = 70,
    scroll_lock = 71,
    pause = 72,
    insert = 73,
    home = 74,
    page_up = 75,
    delete = 76,
    end = 77,
    page_down = 78,
    right = 79,
    left = 80,
    down = 81,
    up = 82,
    num_lock_clear = 83,
    kp_divide = 84,
    kp_multiply = 85,
    kp_minus = 86,
    kp_plus = 87,
    kp_enter = 88,
    kp_1 = 89,
    kp_2 = 90,
    kp_3 = 91,
    kp_4 = 92,
    kp_5 = 93,
    kp_6 = 94,
    kp_7 = 95,
    kp_8 = 96,
    kp_9 = 97,
    kp_0 = 98,
    kp_period = 99,
    non_us_backslash = 100,
    application = 101,
    power = 102,
    kp_equals = 103,
    f13 = 104,
    f14 = 105,
    f15 = 106,
    f16 = 107,
    f17 = 108,
    f18 = 109,
    f19 = 110,
    f20 = 111,
    f21 = 112,
    f22 = 113,
    f23 = 114,
    f24 = 115,
    execute = 116,
    help = 117,
    menu = 118,
    select = 119,
    stop = 120,
    again = 121,
    undo = 122,
    cut = 123,
    copy = 124,
    paste = 125,
    find = 126,
    mute = 127,
    volume_up = 128,
    volume_down = 129,
    kp_comma = 133,
    kp_equalsas400 = 134,
    international1 = 135,
    international2 = 136,
    international3 = 137,
    international4 = 138,
    international5 = 139,
    international6 = 140,
    international7 = 141,
    international8 = 142,
    international9 = 143,
    lang1 = 144,
    lang2 = 145,
    lang3 = 146,
    lang4 = 147,
    lang5 = 148,
    lang6 = 149,
    lang7 = 150,
    lang8 = 151,
    lang9 = 152,
    alterase = 153,
    sysreq = 154,
    cancel = 155,
    clear = 156,
    prior = 157,
    enter2 = 158,
    separator = 159,
    out = 160,
    oper = 161,
    clear_again = 162,
    crsel = 163,
    exsel = 164,
    kp_00 = 176,
    kp_000 = 177,
    thousands_separator = 178,
    decimal_separator = 179,
    currency_unit = 180,
    currency_subunit = 181,
    kp_left_paren = 182,
    kp_right_paren = 183,
    kp_left_brace = 184,
    kp_right_brace = 185,
    kp_tab = 186,
    kp_backspace = 187,
    kp_a = 188,
    kp_b = 189,
    kp_c = 190,
    kp_d = 191,
    kp_e = 192,
    kp_f = 193,
    kp_xor = 194,
    kp_power = 195,
    kp_percent = 196,
    kp_less = 197,
    kp_greater = 198,
    kp_ampersand = 199,
    kp_double_ampersand = 200,
    kp_vertical_bar = 201,
    kp_double_vertical_bar = 202,
    kp_colon = 203,
    kp_hash = 204,
    kp_space = 205,
    kp_at = 206,
    kp_exclam = 207,
    kp_mem_store = 208,
    kp_mem_recall = 209,
    kp_mem_clear = 210,
    kp_mem_add = 211,
    kp_mem_subtract = 212,
    kp_mem_multiply = 213,
    kp_mem_divide = 214,
    kp_plus_minus = 215,
    kp_clear = 216,
    kp_clear_entry = 217,
    kp_binary = 218,
    kp_octal = 219,
    kp_decimal = 220,
    kp_hexadecimal = 221,
    left_ctrl = 224,
    left_shift = 225,
    left_alt = 226,
    left_gui = 227,
    right_ctrl = 228,
    right_shift = 229,
    right_alt = 230,
    right_gui = 231,
    mode = 257,
    audio_next = 258,
    audio_previous = 259,
    audio_stop = 260,
    audio_play = 261,
    audio_mute = 262,
    media_select = 263,
    www = 264,
    mail = 265,
    calculator = 266,
    computer = 267,
    ac_search = 268,
    ac_home = 269,
    ac_back = 270,
    ac_forward = 271,
    ac_stop = 272,
    ac_refresh = 273,
    ac_bookmarks = 274,
    brightness_down = 275,
    brightness_up = 276,
    display_switch = 277,
    kbd_illumination_toggle = 278,
    kbd_illumination_down = 279,
    kbd_illumination_up = 280,
    eject = 281,
    sleep = 282,
    app1 = 283,
    app2 = 284,
    audio_rewind = 285,
    audio_fast_forward = 286,
    num_scancodes = 512,
    _,
};
