
const std = @import("std");
const lib = @import("../../lib.zig");
const core = @import("../../core.zig");

const Graphics = @This();
const Color = core.Color;
const Vec2 = core.Vec2;
const Rect = core.Rect;
const Texture = core.Texture;
const Viewport = core.Engine.Viewport;



pub const GraphicsError = error {
} 
|| lib.SDLError;



pub const Sprite = struct {
    texture: Texture,
  source: Rect,
  dest: Rect,
  angle: f32,
  center: Vec2,
  flip_h: bool,
  flip_v: bool,
  modulate: Color,
};



renderer: *lib.SDL_Renderer,
backbuffer: ?Texture,
viewport: Viewport,
viewport_enabled: bool,



pub fn init(
  window: *lib.SDL_Window
) GraphicsError!Graphics {
  var renderer = try lib.checkSDLPointer(
    lib.SDL_CreateRenderer(
      window, -1, 
      lib.SDL_RENDERER_ACCELERATED | lib.SDL_RENDERER_TARGETTEXTURE
    )
  );

  return Graphics {
    .renderer = renderer,
    .backbuffer = null,
    .viewport = Viewport.init(),
    .viewport_enabled = true,
  };
}

pub fn destroy(
  self: *Graphics
) void {
  if( self.backbuffer ) |*buf|
    buf.destroy();
  
  lib.SDL_DestroyRenderer(self.renderer);
}



pub fn getSize(
  self: *const Graphics
) Vec2 {
  return if( self.backbuffer ) |buf|
    buf.getSize()
  else
    Vec2.zero;
}

pub fn setSize(
  self: *Graphics, 
  width: u32, height: u32
) GraphicsError!void {
  if( self.backbuffer ) |*buf|
    buf.destroy();
  
  self.backbuffer = try Texture.create(width, height);
  self.target(self.backbuffer.?);
}

pub fn getScreenSize(
  self: *Graphics
) Vec2 {
  var width: c_int = 0;
  var height: c_int = 0;

  var render_target = lib.SDL_GetRenderTarget(self.renderer);
  defer _ = lib.SDL_SetRenderTarget(self.renderer, render_target);

  self.targetScreen();

  _ = lib.SDL_GetRendererOutputSize(self.renderer, &width, &height);
  
  return Vec2.init(@intToFloat(f32, width), @intToFloat(f32, height));
}



pub fn getViewportRectangle(
  self: *const Graphics
) Rect {
  return self.viewport.getRectangle();
}

pub fn getViewportPosition(
  self: *const Graphics
) Vec2 {
  return self.viewport.getRectangle().position;
}

pub fn getViewportSize(
  self: *const Graphics
) Vec2 {
  return self.viewport.getRectangle().size;
}

pub fn getViewportCenter(
  self: *const Graphics
) Vec2 {
  return self.viewport.getCenter();
}



pub fn transformPoint(
  self: *Graphics, 
  x: f32, y: f32
) Vec2 {
  return if( self.viewport_enabled )
    self.viewport.transformPoint(x, y)
  else
    Vec2.init(x, y);
}

pub fn transformPointV(
  self: *Graphics, 
  vector: Vec2
) Vec2 {
  return self.transformPoint(vector.x, vector.y);
}



pub fn transformRect(
  self: *Graphics, 
  x: f32, y: f32, 
  width: f32, height: f32
) Rect {
  return if( self.viewport_enabled ) 
    self.viewport.transformRect(
      x, y, 
      width, height
    )
  else
    Rect.init(
      x, y, 
      width, height
    );
}

pub fn transformRectR(
  self: *Graphics, 
  rectangle: Rect
) Rect {
  return self.transformRect(
    rectangle.position.x, rectangle.position.y, 
    rectangle.size.x, rectangle.size.y
  );
}



pub fn target(
  self: *Graphics, 
  opt_texture: ?Texture,
) void {
  _ = if( opt_texture ) |texture|
    lib.SDL_SetRenderTarget(self.renderer, texture.texture)
  else
    lib.SDL_SetRenderTarget(self.renderer, self.backbuffer.?.texture);
    
}

pub fn targetScreen(
  self: *Graphics
) void {
  _ = lib.SDL_SetRenderTarget(self.renderer, null);
}



pub fn startFrame(
    self: *Graphics
) GraphicsError!void {
  if( self.backbuffer == null )
    try self.setSize(1280, 720);

  self.viewport_enabled = true;
  self.target(self.backbuffer.?);
}

pub fn endFrame(
  self: *Graphics
) void {
  const backbuffer_size = self.getSize();
  const screen_size = self.getScreenSize();
  const scale = std.math.min(
    screen_size.x / backbuffer_size.x,
    screen_size.y / backbuffer_size.y 
  );
  const scaled_size = backbuffer_size.scale(scale);
  const position = Vec2.init(
    screen_size.x / 2 - scaled_size.x / 2,
    screen_size.y / 2 - scaled_size.y / 2
  );

  self.viewport_enabled = false;
  
  self.targetScreen();
  self.clear(0, 0, 0, 255);
  self.spriteV(
    self.backbuffer.?,
    position, scaled_size,
    Vec2.zero, backbuffer_size,
    0, Vec2.zero, false, false, 255, 255, 255, 255
  );
}

pub fn present(
  self: *Graphics
) void {
  lib.SDL_RenderPresent(self.renderer);
}



pub fn clear(
  self: *Graphics, 
  r: u8, g: u8, b: u8, a: u8
) void {
  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderClear(self.renderer);
}

pub fn clearC(
  self: *Graphics, 
  color: Color
) void {
  self.clear(color.r, color.g, color.b, color.a);
}



pub fn point(
  self: *Graphics, 
  x: f32, y: f32, 
  r: u8, g: u8, b: u8, a: u8
) void {
  const p = self.transformPoint(x, y);

  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderDrawPointF(self.renderer, p.x, p.y);
}

pub fn pointV(
  self: *Graphics,
  vector: Vec2,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.point(
      vector.x, vector.y, 
      r, g, b, a
  );
}

pub fn pointC(
  self: *Graphics,
  x: f32, y: f32,
  color: Color
) void {
  self.point(
    x, y, 
    color.r, color.g, color.b, color.a
  );
}

pub fn pointVC(
  self: *Graphics,
  vector: Vec2, 
  color: Color
) void {
  self.point(vector.x, vector.y, color.r, color.g, color.b, color.a);
}



pub fn line(
  self: *Graphics,
  x0: f32, y0: f32,
  x1: f32, y1: f32,
  r: u8, g: u8, b: u8, a: u8
) void {
  const p0 = self.transformPoint(x0, y0);
  const p1 = self.transformPoint(x1, y1);

  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderDrawLineF(self.renderer, p0.x, p0.y, p1.x, p1.y);
}

pub fn lineV(
  self: *Graphics,
  vector0: Vec2,
  vector1: Vec2,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.line(
    vector0.x, vector0.y, 
    vector1.x, vector1.y, 
    r, g, b, a
  );
}

pub fn lineC(
  self: *Graphics,
  x0: f32, y0: f32,
  x1: f32, y1: f32,
  color: Color
) void {
  self.line(
    x0, y0, 
    x1, y1, 
    color.r, color.g, color.b, color.a
  );
}

pub fn lineVC(
  self: *Graphics,
  vector0: Vec2,
  vector1: Vec2,
  color: Color
) void {
  self.line(
    vector0.x, vector0.y, 
    vector1.x, vector1.y, 
    color.r, color.g, color.b, color.a
  );
}



pub fn border(
  self: *Graphics,
  x: f32, y: f32, 
  w: f32, h: f32,
  r: u8, g: u8, b: u8, a: u8
) void {
  const xformed_rect = self.transformRect(x, y, w, h);
  const rect = lib.SDL_FRect {
    .x = xformed_rect.position.x, .y = xformed_rect.position.y, 
    .w = xformed_rect.size.x, .h = xformed_rect.size.y
  };

  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderDrawRectF(self.renderer, &rect);
}

pub fn borderV(
  self: *Graphics,
  position: Vec2, 
  size: Vec2,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.border(
    position.x, position.y, 
    size.x, size.y, 
    r, g, b, a
  );
}

pub fn borderR(
  self: *Graphics,
  rectangle: Rect,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.border(
    rectangle.position.x, rectangle.position.y, 
    rectangle.size.x, rectangle.size.y,
    r, g, b, a
  );
}

pub fn borderC(
  self: *Graphics,
  x: f32, y: f32, 
  w: f32, h: f32,
  color: Color
) void {
  self.border(
    x, y, 
    w, h, 
    color.r, color.g, color.b, color.a
  );
}

pub fn borderVC(
  self: *Graphics,
  position: Vec2, 
  size: Vec2,
  color: Color
) void {
  self.border(
    position.x, position.y, 
    size.x, size.y, 
    color.r, color.g, color.b, color.a
  );
}

pub fn borderRC(
  self: *Graphics,
  rectangle: Rect,
  color: Color
) void {
  self.border(
    rectangle.position.x, rectangle.position.y, 
    rectangle.size.x, rectangle.size.y,
    color.r, color.g, color.b, color.a
  );
}


pub fn solid(
  self: *Graphics,
  x: f32, y: f32, 
  width: f32, height: f32,
  r: u8, g: u8, b: u8, a: u8
) void {
  const xformed_rect = self.transformRect(x, y, width, height);
  const rect = lib.SDL_FRect {
    .x = xformed_rect.position.x, .y = xformed_rect.position.y, 
    .w = xformed_rect.size.x, .h = xformed_rect.size.y
  };

  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderFillRectF(self.renderer, &rect);
}

pub fn solidV(
  self: *Graphics,
  position: Vec2, 
  size: Vec2,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.solid(
    position.x, position.y, 
    size.x, size.y, 
    r, g, b, a
  );
}

pub fn solidR(
  self: *Graphics,
  rectangle: Rect,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.solid(
    rectangle.position.x, rectangle.position.y, 
    rectangle.size.x, rectangle.size.y,
    r, g, b, a
  );
}

pub fn solidC(
  self: *Graphics,
  x: f32, y: f32, 
  width: f32, height: f32,
  color: Color
) void {
  self.solid(
    x, y, 
    width, height, 
    color.r, color.g, color.b, color.a
  );
}

pub fn solidVC(
  self: *Graphics,
  position: Vec2, 
  size: Vec2,
  color: Color
) void {
  self.solid(
    position.x, position.y, 
    size.x, size.y, 
    color.r, color.g, color.b, color.a
  );
}

pub fn solidRC(
  self: *Graphics,
  rectangle: Rect,
  color: Color
) void {
  self.solid(
    rectangle.position.x, rectangle.position.y, 
    rectangle.size.x, rectangle.size.y,
    color.r, color.g, color.b, color.a
  );
}


pub fn sprite(
  self: *Graphics,
  texture: Texture,
  dest_x: f32, dest_y: f32, dest_width: f32, dest_height: f32,
  src_x: f32, src_y: f32, src_width: f32, src_height: f32,
  angle: f32,
  center_x: f32, center_y: f32, 
  flip_h: bool, flip_v: bool,
  r: u8, g: u8, b: u8, a: u8
) void {
  const xformed_rect = self.transformRect(dest_x, dest_y, dest_width, dest_height);

  const source_rect = lib.SDL_Rect { 
    .x = @floatToInt(c_int, src_x), .y = @floatToInt(c_int, src_y), 
    .w = @floatToInt(c_int, src_width), .h = @floatToInt(c_int, src_height) 
  };
  const destination_rect = lib.SDL_FRect { 
    .x = xformed_rect.position.x, .y = xformed_rect.position.y, 
    .w = xformed_rect.size.x, .h = xformed_rect.size.y 
  };
  const center = lib.SDL_FPoint { 
    .x = center_x, .y = center_y 
  };

  _ = lib.SDL_SetRenderDrawColor(self.renderer, r, g, b, a);
  _ = lib.SDL_RenderCopyExF(
    self.renderer, texture.texture, 
    &source_rect, &destination_rect, angle, &center, 
    @intCast(c_uint, (if( flip_h ) lib.SDL_FLIP_HORIZONTAL else 0) | (if( flip_v ) lib.SDL_FLIP_VERTICAL else 0))
  );
}

pub fn spriteV(
  self: *Graphics,
  texture: Texture,
  dest_position: Vec2, dest_size: Vec2,
  source_position: Vec2, source_size: Vec2,
  angle: f32,
  center: Vec2,
  flip_h: bool, flip_v: bool,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.sprite(
    texture, 
    dest_position.x, dest_position.y, dest_size.x, dest_size.y, 
    source_position.x, source_position.y, source_size.x, source_size.y, 
    angle, 
    center.x, center.y, 
    flip_h, flip_v, 
    r, g, b, a
  );
}

pub fn spriteR(
  self: *Graphics,
  texture: Texture,
  destination: Rect,
  source: Rect,
  angle: f32,
  center_x: f32, center_y: f32,
  flip_h: bool, flip_v: bool,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.sprite(
    texture, 
    destination.position.x, destination.position.y, destination.size.x, destination.size.y, 
    source.position.x, source.position.y, source.size.x, source.size.y, 
    angle, 
    center_x, center_y, 
    flip_h, flip_v, 
    r, g, b, a
  );
}

pub fn spriteC(
  self: *Graphics,
  texture: Texture,
  dest_x: f32, dest_y: f32, dest_width: f32, dest_height: f32,
  src_x: f32, src_y: f32, src_width: f32, src_height: f32,
  angle: f32,
  center_x: f32, center_y: f32, 
  flip_h: bool, flip_v: bool,
  color: Color
) void {
  self.sprite(
    texture, 
    dest_x, dest_y, dest_width, dest_height, 
    src_x, src_y, src_width, src_height, 
    angle, 
    center_x, center_y, 
    flip_h, flip_v, 
    color.r, color.g, color.b, color.a
  );
}

pub fn spriteRV(
  self: *Graphics,
  texture: Texture,
  destination: Rect,
  source: Rect,
  angle: f32,
  center: Vec2,
  flip_h: bool, flip_v: bool,
  r: u8, g: u8, b: u8, a: u8
) void {
  self.sprite(
    texture, 
    destination.position.x, destination.position.y, destination.size.x, destination.size.y, 
    source.position.x, source.position.y, source.size.x, source.size.y, 
    angle, 
    center.x, center.y, 
    flip_h, flip_v, 
    r, g, b, a
  );
}

pub fn spriteVC(
  self: *Graphics,
  texture: Texture,
  dest_position: Vec2, dest_size: Vec2,
  source_position: Vec2, source_size: Vec2,
  angle: f32,
  center: Vec2,
  flip_h: bool, flip_v: bool,
  color: Color
) void {
  self.sprite(
    texture, 
    dest_position.x, dest_position.y, dest_size.x, dest_size.y, 
    source_position.x, source_position.y, source_size.x, source_size.y, 
    angle, 
    center.x, center.y, 
    flip_h, flip_v, 
    color.r, color.g, color.b, color.a
  );
}

pub fn spriteRC(
  self: *Graphics,
  texture: Texture,
  destination: Rect,
  source: Rect,
  angle: f32,
  center_x: f32, center_y: f32,
  flip_h: bool, flip_v: bool,
  color: Color
) void {
  self.sprite(
    texture, 
    destination.position.x, destination.position.y, destination.size.x, destination.size.y, 
    source.position.x, source.position.y, source.size.x, source.size.y, 
    angle, 
    center_x, center_y, 
    flip_h, flip_v, 
    color.r, color.g, color.b, color.a
  );
}

pub fn spriteRVC(
  self: *Graphics,
  texture: Texture,
  destination: Rect,
  source: Rect,
  angle: f32,
  center: Vec2,
  flip_h: bool, flip_v: bool,
  color: Color
) void {
  self.sprite(
    texture, 
    destination.position.x, destination.position.y, destination.size.x, destination.size.y, 
    source.position.x, source.position.y, source.size.x, source.size.y, 
    angle, 
    center.x, center.y, 
    flip_h, flip_v, 
    color.r, color.g, color.b, color.a
  );
}

pub fn spriteO(
  self: *Graphics,
  sprite_obj: Sprite
) void {
  self.spriteRVC(
    sprite_obj.texture,
    sprite_obj.dest,
    sprite_obj.source,
    sprite_obj.angle,
    sprite_obj.center,
    sprite_obj.flip_h, sprite_obj.flip_v,
    sprite_obj.modulate
  );
}
