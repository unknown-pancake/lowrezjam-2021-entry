
const std = @import("std");
const Array = std.ArrayList;



pub fn FSM(comptime E: type) type {
  const count = @typeInfo(E).Enum.fields.len;

  return struct {
    const Self = @This();

    pub const Fn = fn(fsm: *Self) anyerror!void;

    pub const State = struct {
      enter_fn: ?Fn = null,
      resume_fn: ?Fn = null,
      pause_fn: ?Fn = null,
      exit_fn: ?Fn = null,
      interupt_fn: ?Fn = null,
      tick_fn: ?Fn = null,
      render_fn: ?Fn = null
    };


    states: [count]State,
    stack: Array(E),


    pub fn init() Self {
      var self = Self {
        .states = undefined,
        .stack = Array(E).init(std.heap.c_allocator)
      };

      for( self.states ) |*s| {
        s.enter_fn = null;
        s.resume_fn = null;
        s.pause_fn = null;
        s.exit_fn = null;
        s.interupt_fn = null;
        s.tick_fn = null;
        s.render_fn = null;
      }

      return self;
    }

    pub fn destroy(self: *Self) void {
      self.stack.deinit();
    }


    pub fn setState(
      self: *Self, s: E, state: State
    ) void {
      self.states[@enumToInt(s)] = state;
    }

    pub fn getState(self: *Self, s: E) *State {
      return &self.states[@enumToInt(s)];
    }

    pub fn getCurrentState(self: *Self) ?*State {
      if( self.stack.items.len > 0)
        return self.getState(self.stack.items[self.stack.items.len - 1]);
      
      return null;
    }


    pub fn push(self: *Self, s: E) anyerror!void {
      if( self.getCurrentState() ) |last| 
        if( last.pause_fn ) |f| try f(self);

      var state = self.getState(s);
      try self.stack.append(s);

      if( state.enter_fn ) |f| try f(self);
      if( state.resume_fn ) |f| try f(self);
    }

    pub fn pop(self: *Self) anyerror!void {
      var last = self.getState( self.stack.pop() );
      if( last.pause_fn ) |f| try f(self);
      if( last.exit_fn ) |f| try f(self);

      if( self.getCurrentState() ) |s| 
        if( s.resume_fn ) |f| try f(self);
    }

    pub fn switchTo(self: *Self, s: E) anyerror!void {
      var last = self.getState( self.stack.pop() );
      if( last.pause_fn ) |f| try f(self);
      if( last.exit_fn ) |f| try f(self);

      var state = self.getState(s);
      try self.stack.append(s);

      if( state.enter_fn ) |f| try f(self);
      if( state.resume_fn ) |f| try f(self);
    }



    pub fn tick(self: *Self) anyerror!void {
      if( self.getCurrentState() ) |s|
        if( s.tick_fn ) |f| 
          try f(self);
    }

    pub fn render(self: *Self) anyerror!void {
      if( self.getCurrentState() ) |s|
        if( s.render_fn ) |f| 
          try f(self);
    }

  };
}
