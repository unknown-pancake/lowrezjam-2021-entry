
const Vec2 = @import("./vec2.zig");
const Rect = @This();


position: Vec2,
size: Vec2,


pub fn init(x: f32, y: f32, w: f32, h: f32) Rect {
    return Rect {
        .position = Vec2.init(x, y),
        .size = Vec2.init(w, h)
    };
}

pub fn initV(p: Vec2, s: Vec2) Rect {
    return Rect {
        .position = p,
        .size = s
    };
}


pub fn getCenter(self: Rect) Vec2 {
    return Vec2.init(
        self.position.x + self.size.x/2,
        self.position.y + self.size.y/2
    );
}

pub fn setCenter(self: *Rect, c: Vec2) void {
    self.position.x = c.x - self.size.x/2;
    self.position.y = c.y - self.size.y/2;
}

pub fn withCenter(self: Rect, c: Vec2) void {
    self.setCenter(c);
    return self;
}
