
const std = @import("std");
const core = @import("../core.zig");

const AsepriteAnim = @This();
const Aseprite = core.Aseprite;
const Texture = core.Texture;
const Graphics = core.Engine.Graphics;
const Engine = core.Engine;
const Vec2 = core.Vec2;
const Rect = core.Rect;
const Color = core.Color;



aseprite: Aseprite,
texture: Texture,
sprite: Graphics.Sprite,
current_tag: Aseprite.FrameTag,
frame_duration: i32,
frame: u32,



pub fn init(path: []const u8, initanim: []const u8, size: Vec2) anyerror!AsepriteAnim {
  var aseprite = try Aseprite.init(path);
  errdefer aseprite.destroy();

  var tex = try aseprite.loadTexture();
  errdefer tex.destroy();

  var tag = aseprite.findFrameTags(initanim).?;
  var spr = Graphics.Sprite {
    .texture = tex,
    .source = Rect.init(0, 0, 0, 0),
    .dest = Rect.initV(size.neg().scale(0.5), size),
    .angle = 0,
    .center = size.scale(0.5),
    .flip_h = false,
    .flip_v = false,
    .modulate = Color.init(255, 255, 255, 255),
  };

  aseprite.fetchFrame(tag.from, &spr);

  return AsepriteAnim {
    .aseprite = aseprite,
    .texture = tex,
    .sprite = spr,
    .current_tag = tag,
    .frame_duration = @intCast(i32, aseprite.getFrameDuration(tag.from)),
    .frame = tag.from
  };
}

pub fn destroy(self: *AsepriteAnim) void {
  self.aseprite.destroy();
  self.texture.destroy();
}



pub fn tick(self: *AsepriteAnim) void {
  self.frame_duration -= @intCast(i32, std.time.ns_per_s/60);

  if( self.frame_duration < 0 ) {
    self.frame += 1; 
    if( self.frame > self.current_tag.to ) {
      self.frame = self.current_tag.from;
    }

    self.frame_duration = @intCast(i32, self.aseprite.getFrameDuration(self.frame));
    self.aseprite.fetchFrame(self.frame, &self.sprite);
  }
}

pub fn render(self: *AsepriteAnim) void {
  Engine.graphics().spriteO(self.sprite);
}



pub fn switchTo(self: *AsepriteAnim, anim: []const u8) void {
  self.current_tag = self.aseprite.findFrameTags(anim).?;
  self.aseprite.fetchFrame(self.current_tag.from, &self.sprite);
  self.frame = self.current_tag.from;
  self.frame_duration = @intCast(i32, self.aseprite.getFrameDuration(self.frame));
}



pub fn setPosition(self: *AsepriteAnim, pos: Vec2) void {
  var spr = &self.sprite;

  spr.dest.position = pos.sub(spr.dest.size.scale(0.5));
}
